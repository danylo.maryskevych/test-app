const express = require('express');
const app = express();

// Environment variables
const port = process.env.PORT || 3000;
const country_code = process.env.COUNTRY_CODE || 'US';
const scheduler = process.env.SCHEDULER || 'default_scheduler';
const app_name = process.env.APP_NAME || 'my_app';

// Define a simple route
app.get('/', (req, res) => {
  res.send(`Hello, this is your web app!\nCountry Code: ${country_code}\nScheduler: ${scheduler}\nApp Name: ${app_name}`);
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});